%define name %(echo $(${CI_PROJECT_DIR}/package.sh name))
%define version %(echo $(${CI_PROJECT_DIR}/package.sh version))
%define release %(echo $(${CI_PROJECT_DIR}/package.sh release))
%global modname %(echo %{name} | awk -F 'python3-' '{print $2}')
%define install_dirname %(echo %{modname} | sed 's/python//;s/-//g')
%define eggdir %(echo %{modname} | tr '-' '_')
%global debug_package %{nil}

Name:           %{name}
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Google API Client Library for Python
License:        Apache Software License (Apache 2.0)
URL:            https://pypi.org/project/tzlocal
Source0:        %{name}-%{version}.tar.gz
BuildArch:      x86_64

%?python_enable_dependency_generator

%description
This is the Google API Python client library for Google's discovery based APIs. To get started, please see the docs folder.

This library is considered complete and is in maintenance mode. This means that we will address critical bugs and security issues but will not add any new features.

This library is officially supported by Google. However, the maintainers of this repository recommend using Cloud Client Libraries for Python, where possible, for new code development. For more information, please visit Client Libraries Explained.

%prep
%autosetup -n %{modname}-%{version}

%build
%py3_build

%install
%py3_install

%check

%files -n %{name}
%{python3_sitelib}/apiclient/
%{python3_sitelib}/%{install_dirname}/
%{python3_sitelib}/%{eggdir}-%{version}*

%changelog
* Fri Mar 31 2023 Gayan Munasinghe <gayan@outlook.com>
  - Initial rpm release
