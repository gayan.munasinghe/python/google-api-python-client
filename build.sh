#!/bin/bash

set -x

PYTHON_PACKAGE_NAME=$(bash package.sh name)
PACKAGE_NAME=$(echo $PYTHON_PACKAGE_NAME | awk -F 'python3-' '{print $2}')
VERSION=$(bash package.sh version)
cp sources/${PACKAGE_NAME}-${VERSION}.tar.gz ~/rpmbuild/SOURCES/${PYTHON_PACKAGE_NAME}-${VERSION}.tar.gz
rpmbuild -bb *.spec
